﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ReactionTest
{
    public partial class Form1 : Form
    {
        public Stopwatch timer { get; set; }
        public Stopwatch timer1 { get; set; }

        Random randomNumberGen = new Random();


        public Form1()
        {
            InitializeComponent();
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            

            

            start();
            
        }
        public string TextboxText
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }
        public void start()
        {
            timer = Stopwatch.StartNew();
            BackColor = Color.Yellow;

            int countDown = randomNumberGen.Next(3, 7) * 1000;

            while (true)
            {
                if (timer.ElapsedMilliseconds > countDown)
                {
                    BackColor = Color.Green;

                    break;
                }
            }
        }
    }
}
