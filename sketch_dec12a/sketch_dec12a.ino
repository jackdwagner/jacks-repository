#include<Servo.h>

Servo serX;
Servo serY;
int relayPin = 9;
String serialData;

void setup() {

  serX.attach(10);
  serY.attach(11);
  Serial.begin(9600);
  Serial.setTimeout(10);
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, HIGH);
}

void loop() {
  //lol
}

void serialEvent() {
  serialData = Serial.readString();
  if (serialData.indexOf('X') != -1) {
    serX.write(parseDataX(serialData));
    serY.write(parseDataY(serialData));
  } else if (serialData.indexOf('T') != -1){
    digitalWrite(relayPin, LOW);
  } else {
    digitalWrite(relayPin, HIGH);
  }
  
}

int parseDataX(String data){
  data.remove(data.indexOf("Y"));
  data.remove(data.indexOf("X"), 1);
  return data.toInt();
}

int parseDataY(String data){
  data.remove(0,data.indexOf("Y") + 1);
  return data.toInt();
}
