﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace LaserTurret
{
    public partial class Form1 : Form
    {
        public Stopwatch watch { get; set; }

        bool laserState = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            watch = Stopwatch.StartNew();
            port.Open();
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            writeToPort(new Point (e.X, e.Y));
        }
        public void writeToPort(Point coordinates)
        {
           
            
            if (watch.ElapsedMilliseconds > 15)
            {
                watch = Stopwatch.StartNew();
                var stringToWrite = String.Format("X{0}Y{1}",
                 (180 - coordinates.X / (Size.Width / 180)),
                 (180 - coordinates.Y / (Size.Height / 180)));
                port.Write(stringToWrite);
            }

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                laserState = !laserState;
                
                string stringToWrite = laserState.ToString();
                port.Write(stringToWrite);
            }
        }
    }
}
